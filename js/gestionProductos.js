var productosObtenidos;

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    //que el estatus de peticion sea respondido y que haya concluido OK
    if (this.readyState == 4 && this.status == 200) {
      //el texto que esta en el campo value es el que se parseara y se enviara a la consola
      //console.table(JSON.parse(request.responseText).value);
      // asignar a la variable prpoductos Obtenidos el JSON donde estan los productos
      productosObtenidos =request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  //se envia la peticion
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //document es el objeto que tiene toda nuestra pagina
  var divTabla = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //validar que se haya convertido bien el JSON y que ponga en una alerta el nombre del
  //producto del primer elemento del arreglo
  //alert(JSONProductos.value[0].ProductName);
  //trae todo el JSOn del primer producto
  //alert(JSONProductos.value[0]);
  //muestra todos los nombres de los productos
  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    //pintamos la tabla, tr=tablerow, td=tabledata
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsinStock;
    // se agregan las columnas a las filas
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    // se agrega el row al tbody
    tbody.appendChild(nuevaFila);
  }

  //agregamos a la tabla el tbody
  tabla.appendChild(tbody);
  //a la variable div se le agrega la tabla
  divTabla.appendChild(tabla);
}
