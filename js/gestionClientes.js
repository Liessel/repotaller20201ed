function getClientes() {
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    //que el estatus de peticion sea respondido y que haya concluido OK
    if (this.readyState == 4 && this.status == 200) {
      //el texto que esta en el campo value es el que se parseara y se enviara a la consola
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos =request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  //se envia la peticion
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  //document es el objeto que tiene toda nuestra pagina
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //validar que se haya convertido bien el JSON y que ponga en una alerta el nombre del
  //producto del primer elemento del arreglo
  //alert(JSONClientes.value[0].CustomerID);
  //trae todo el JSOn del primer cliente
  //alert(JSONClientes.value[0]);
  //muestra todos los nombres de los clientes

  //ruta inicial
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].CustomerID);
    //pintamos la tabla, tr=tablerow, td=tabledata
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    //var columnaPais = document.createElement("td");
    //columnaPais.innerText = JSONClientes.value[i].Country;
    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");

    if (JSONClientes.value[i].Country == "UK") {
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    } else {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    imgBandera.style.width = "70px";
    imgBandera.style.height = "40px";
    columnaBandera.appendChild(imgBandera);

    // se agregan las columnas a las filas
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    //nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaBandera);

    // se agrega el row al tbody
    tbody.appendChild(nuevaFila);
  }

  //agregamos a la tabla el tbody
  tabla.appendChild(tbody);
  //a la variable div se le agrega la tabla
  divTabla.appendChild(tabla);
}
